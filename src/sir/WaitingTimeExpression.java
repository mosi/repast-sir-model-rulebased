package sir;

@FunctionalInterface
public interface WaitingTimeExpression {
	
	double get();

}
