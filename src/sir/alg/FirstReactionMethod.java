package sir.alg;

import java.util.List;

import repast.simphony.engine.environment.RunEnvironment;
import repast.simphony.engine.schedule.IAction;
import repast.simphony.engine.schedule.ISchedule;
import repast.simphony.engine.schedule.ScheduleParameters;
import sir.Agent;
import sir.Rule;

public class FirstReactionMethod implements SimulationAlgorithm {

	private ISchedule schedule;
	private List<Agent> agents;

	public FirstReactionMethod(List<Agent> agents) {
		super();
		this.schedule = RunEnvironment.getInstance().getCurrentSchedule();
		this.agents = agents;
	}

	@Override
	public void start() {
		schedule.schedule(ScheduleParameters.createOneTime(0), this,
				"scheduleNextEvent");
	}

	public void executeAndSchedule(IAction actionToExecute) {

		if (actionToExecute != null) {
			actionToExecute.execute();
		}

		scheduleNextEvent();
	}

	public void scheduleNextEvent() {

		double currentTime = schedule.getTickCount();
		double minWaitingTime = Double.POSITIVE_INFINITY;
		IAction nextAction = null;

		for (Agent agent : agents) {
			for (Rule rule : agent.getRules()) {
				if (rule.isApplicable()) {
					double waitingTime = rule.getWaitingTime();
					if (waitingTime < minWaitingTime) {
						minWaitingTime = waitingTime;
						nextAction = rule.getEffect();
					}
				}
			}
		}

		if (minWaitingTime != Double.POSITIVE_INFINITY) {
			schedule.schedule(
					ScheduleParameters.createOneTime(currentTime
							+ minWaitingTime), this, "executeAndSchedule",
					nextAction);
		}
	}

}
