package sir.alg;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import repast.simphony.engine.environment.RunEnvironment;
import repast.simphony.engine.schedule.IAction;
import repast.simphony.engine.schedule.ISchedulableAction;
import repast.simphony.engine.schedule.ISchedule;
import repast.simphony.engine.schedule.ScheduleParameters;
import repast.simphony.space.graph.Network;
import sir.Agent;
import sir.Rule;

public class NextReactionMethod implements SimulationAlgorithm {

	private ISchedule schedule;
	private List<Agent> agents;
	private Network<Agent> network;
	private Map<Agent, ISchedulableAction> scheduledEvent;

	public NextReactionMethod(List<Agent> agents, Network<Agent> network) {
		super();
		this.agents = agents;
		this.network = network;
		this.scheduledEvent = new HashMap<>();
		schedule = RunEnvironment.getInstance().getCurrentSchedule();
	}

	@Override
	public void start() {
		schedule.schedule(ScheduleParameters.createOneTime(0), this,
				"scheduleEventsFor", agents);
	}

	public void scheduleEventsFor(Iterable<Agent> agents) {

		for (Agent agent : agents) {
			scheduleEventForAgent(agent);
		}
	}

	public void executeEvent(Agent agent, IAction actionToExecute) {

		if (actionToExecute != null) {
			actionToExecute.execute();
		}

		// new event for acting agent
		scheduleEventForAgent(agent);

		// reschedule for its neighbours
		scheduleEventsFor(getAffectedAgents(agent));

	}

	private void scheduleEventForAgent(Agent agent) {

		// remove already scheduled event
		ISchedulableAction event = scheduledEvent.get(agent);
		if (event != null) {
			schedule.removeAction(event);
		}

		double currentTime = schedule.getTickCount();
		double minWaitingTime = Double.POSITIVE_INFINITY;
		IAction action = null;

		// schedule event for fastest rule
		for (Rule rule : agent.getRules()) {
			if (rule.isApplicable()) {
				double waitingTime = rule.getWaitingTime();
				if (waitingTime < minWaitingTime) {
					minWaitingTime = waitingTime;
					action = rule.getEffect();
				}
			}
		}

		if (minWaitingTime != Double.POSITIVE_INFINITY) {
			ISchedulableAction scheduledAction = schedule.schedule(
					ScheduleParameters.createOneTime(currentTime
							+ minWaitingTime), this, "executeEvent", agent,
					action);
			scheduledEvent.put(agent, scheduledAction);
		}
	}

	private Iterable<Agent> getAffectedAgents(Agent agent) {

		// TODO generalize this
		return network.getAdjacent(agent);
	}
}
