package sir;

import java.awt.Color;

import repast.simphony.visualizationOGL2D.DefaultStyleOGL2D;

public class AgentStyle extends DefaultStyleOGL2D {

	@Override
	public Color getColor(Object o) {

		if (o instanceof SIRGroovyAgent) {
			SIRGroovyAgent agent = (SIRGroovyAgent) o;
			if (agent.isInfectious())
				return Color.red;
			if (agent.isSusceptible())
				return Color.blue;
			if (agent.isRecovered())
				return Color.green;
		}

		return super.getColor(o);
	}
}
