package sir

import java.util.function.Predicate
import repast.simphony.engine.environment.RunEnvironment;
import repast.simphony.parameter.Parameters;

class SIRGroovyAgent extends GroovyAgent {
	
	Parameters params = RunEnvironment.getInstance().getParameters();
	double infectionRate = params.getDouble("infectionRate");
	double recoverRate = params.getDouble("recoverRate");
	InfectionState infectionState = InfectionState.SUSCEPTIBLE
	
	def SIRGroovyAgent(InfectionState initialInfectionState) {
		this.infectionState = initialInfectionState
		
		addRule(
			condition: {isInfectious()},
			rate: {this.recoverRate},
			effect: {infectionState = InfectionState.RECOVERED}
			)
		
		addRule(
			condition: {isSusceptible()},
			rate: {this.infectionRate * neighbours(SIRGroovyAgent.class).filter({it.isInfectious()} as Predicate).size()},
			effect: {infectionState = InfectionState.INFECTIOUS}
			)
	}
	
	public boolean isSusceptible() {
		return infectionState == InfectionState.SUSCEPTIBLE;
	}

	public boolean isInfectious() {
		return infectionState == InfectionState.INFECTIOUS;
	}

	public boolean isRecovered() {
		return infectionState == InfectionState.RECOVERED;
	}
	
}
