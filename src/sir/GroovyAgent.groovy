package sir;

import org.antlr.stringtemplate.language.ConditionalExpr

public abstract class GroovyAgent extends Agent {

	def addRule(Map conf) {
		def condition = conf.condition
		def rate = conf.rate
		def effect = conf.effect

		super.addRule(
				new Condition() {
					@Override
					public boolean evaluate() {
						condition(this)
					}
				}, new WaitingTimeExpression() {
					@Override
					public double get() {
						exp(rate(this))
					}
				}, new Effect() {
					@Override
					public void execute() {
						effect(this)
					}
				})
	}
}
