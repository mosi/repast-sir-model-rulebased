package sir;

import java.util.ArrayList;
import java.util.List;

import repast.simphony.context.Context;
import repast.simphony.context.space.graph.NetworkBuilder;
import repast.simphony.dataLoader.ContextBuilder;
import repast.simphony.engine.environment.RunEnvironment;
import repast.simphony.parameter.Parameters;
import repast.simphony.random.RandomHelper;
import repast.simphony.space.graph.Network;
import sir.alg.FirstReactionMethod;
import sir.alg.SimulationAlgorithm;

public class SimBuilder implements ContextBuilder<Agent> {
	@Override
	public Context<Agent> build(Context<Agent> context) {

		NetworkBuilder<Agent> netBuilder = new NetworkBuilder<>("network",
				context, false);
		Network<Agent> network = netBuilder.buildNetwork();
		Environment.initialize(network);

		List<Agent> agents = initializePopulation();
		context.addAll(agents);
		createRandomNetwork(context, network);

		SimulationAlgorithm algorithm = new FirstReactionMethod(agents);
//		SimulationAlgorithm algorithm = new NextReactionMethod(agents, network);
		algorithm.start();

		return context;
	}

	private List<Agent> initializePopulation() {

		List<Agent> agents = new ArrayList<>();

		Parameters params = RunEnvironment.getInstance().getParameters();
		int agentCount = params.getInteger("totalAgents");

		int infectedAgentCount = params.getInteger("infectiousAgents");

		for (int i = 1; i <= agentCount - infectedAgentCount; i++) {
			SIRGroovyAgent agent = new SIRGroovyAgent(InfectionState.SUSCEPTIBLE);
			agents.add(agent);
		}

		for (int i = 1; i <= infectedAgentCount; i++) {
			SIRGroovyAgent agent = new SIRGroovyAgent(InfectionState.INFECTIOUS);
			agents.add(agent);
		}

		return agents;
	}

	private void createRandomNetwork(Context<Agent> context,
			Network<Agent> network) {

		int minNeighbours = 1;
		int maxNeighbours = 3;

		for (Agent agent : context.getObjects(Agent.class)) {
			int numNeighbours = RandomHelper.nextIntFromTo(minNeighbours,
					maxNeighbours);
			for (int j = 0; j < numNeighbours; j++) {
				network.addEdge(agent, context.getRandomObject());
			}
		}
	}
}
