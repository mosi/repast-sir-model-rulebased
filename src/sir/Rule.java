package sir;

import repast.simphony.engine.schedule.IAction;

public interface Rule {
	
	boolean isApplicable();
	
	double getWaitingTime();
	
	IAction getEffect();

}
