package sir;

import repast.simphony.space.graph.Network;

public class Environment {

	private static Environment instance = null;

	public static void initialize(Network<Agent> network) {
		instance = new Environment(network);
	}

	public static Environment getInstance() {
		if (instance == null) {
			throw new IllegalStateException();
		}
		return instance;
	}

	private Environment(Network<Agent> network) {
		this.network = network;
	}

	private final Network<Agent> network;

	public Network<Agent> getNetwork() {
		return network;
	}
}
