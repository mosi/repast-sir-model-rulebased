package sir;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import repast.simphony.engine.schedule.IAction;
import repast.simphony.random.RandomHelper;
import repast.simphony.space.graph.Network;

public abstract class Agent {

	private List<Rule> rules = new ArrayList<>();

	protected void addRule(Condition condition,
			WaitingTimeExpression waitingTimeExpression, Effect effect) {

		this.rules.add(new Rule() {

			@Override
			public boolean isApplicable() {
				return condition.evaluate();
			}

			@Override
			public double getWaitingTime() {
				return waitingTimeExpression.get();
			}

			@Override
			public IAction getEffect() {
				return new IAction() {
					@Override
					public void execute() {
						effect.execute();
					}
				};
			}
		});
	}

	protected double exp(double rate) {
		return RandomHelper.createExponential(rate).nextDouble();
	}

	protected <T extends Agent> AgentSet<T> neighbours(Class<T> clazz) {
		AgentSet<T> neighbours = new AgentSet<>();
		for (Agent agent : network().getAdjacent(this)) {
			if (clazz.isInstance(agent)) {
				neighbours.add(clazz.cast(agent));
			}
		}
		return neighbours;
	}

	protected <T extends Agent> AgentSet<T> neighboursWith(Class<T> clazz,
			Predicate<T>... ps) {
		Stream<T> neighbourStream = neighbours(clazz).stream();
		for (Predicate<T> p : ps) {
			neighbourStream = neighbourStream.filter(p);
		}
		return AgentSet.fromStream(neighbourStream);
	}

	private Network<Agent> network() {
		return Environment.getInstance().getNetwork();
	}

	public List<Rule> getRules() {
		return rules;
	}
}
