package sir;

@FunctionalInterface
interface Condition {
	
	boolean evaluate();

}
