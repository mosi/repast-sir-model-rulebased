package sir;

import java.util.HashSet;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class AgentSet<T extends Agent> extends HashSet<T> {

	public static <S extends Agent> AgentSet<S> fromStream(Stream<S> stream) {
		AgentSet<S> agentSet = new AgentSet<>();
		Set<S> agentsInStream = stream.collect(Collectors.toSet());
		agentSet.addAll(agentsInStream);
		return agentSet;
	}

	public AgentSet<T> filter(Predicate<T>... ps) {
		AgentSet<T> agentSet = new AgentSet<>();
		for (T agent : this) {
			boolean retain = true;
			for (Predicate<T> p : ps) {
				if(!p.test(agent)) {
					retain = false;
				}
			}
			if (retain) {
				agentSet.add(agent);
			}
		}
		return agentSet;

	}

}
