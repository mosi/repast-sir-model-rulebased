package sir;

public enum InfectionState {

	SUSCEPTIBLE, INFECTIOUS, RECOVERED;
	
}
