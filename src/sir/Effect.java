package sir;

@FunctionalInterface
interface Effect {
	
	void execute();

}
