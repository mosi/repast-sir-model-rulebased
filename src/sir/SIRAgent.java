package sir;

import repast.simphony.engine.environment.RunEnvironment;
import repast.simphony.parameter.Parameters;

public class SIRAgent extends Agent {

	private InfectionState infectionState;
	private final double infectionRate;
	private final double recoverRate;

	public SIRAgent(InfectionState infectionState) {

		Parameters params = RunEnvironment.getInstance().getParameters();
		infectionRate = params.getDouble("infectionRate");
		recoverRate = params.getDouble("recoverRate");
		this.infectionState = infectionState;

		addRule(() -> this.isInfectious(), 
				() -> exp(recoverRate),
				() -> this.infectionState = InfectionState.RECOVERED);

		addRule(() -> this.isSusceptible(),
				() -> exp(infectionRate
						* neighbours(SIRAgent.class).filter(
								(SIRAgent agent) -> agent.isInfectious())
								.size()),
				() -> this.infectionState = InfectionState.INFECTIOUS);
	}

	public boolean isSusceptible() {
		return infectionState == InfectionState.SUSCEPTIBLE;
	}

	public boolean isInfectious() {
		return infectionState == InfectionState.INFECTIOUS;
	}

	public boolean isRecovered() {
		return infectionState == InfectionState.RECOVERED;
	}
}
